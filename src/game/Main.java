package game;

import java.awt.*;
import java.io.*;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static Saves saves = new Saves();
    public static Scanner robot = new Scanner(System.in);
    public static int[][] move(Point zeroPos,int[][] arr,int i,int j){
        try {
            int a = arr[zeroPos.x][zeroPos.y]; arr[zeroPos.x][zeroPos.y]=arr[zeroPos.x+i][zeroPos.y+j];arr[zeroPos.x+i][zeroPos.y+j] = a;
        }catch (Exception e){

        }
        return arr;
    }

    public static Point zeroPos(int[][] arr){
        Point point =  new Point(0,0);
        for(int i=0;i<arr.length;i++){
            for(int j = 0; j < arr.length;j++){
                if (arr[i][j] == 0) point =new Point(i,j);
            }
        }
        return point;
    }
    public static void printArr(int[][] arr){
        int aLength = arr.length;
        for (int i = 0; i < aLength; i++) {
            for (int j = 0; j < aLength; j++) {
                System.out.printf("%2d ",arr[i][j]);
            }
            System.out.println();
        }
    }

    public static boolean isWin(int[][] arr){
        int aLength = arr.length;
        for (int i = 0; i < aLength; i++) {
            for (int j = 0; j < aLength; j++) {
                if (i != aLength && j != aLength){ if (arr[i][j]!=aLength*i+j+1) return false;}
            }
        }
        return true;
    }
    public static int[][] initArr(int[][] arr){
        int lvl = arr.length;
        for (int i = 0; i < lvl; i++) {
            for (int j = 0; j < lvl; j++) {
                arr[i][j]=lvl*i+j;
            }
        }
        return arr;
    }

    public static int whatLVL(){
        int lvl =4;

        try {
            System.out.println("Write lvl 1-100");
            lvl = Integer.parseInt(robot.next());
        }catch (Exception e) {
            System.out.println("set lvl(4)");
        }
        return lvl;
    }
    public static int[][] mix(int[][] arr){
        int aLength = arr.length;
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {

                int x = random.nextInt(aLength);
                int y = random.nextInt(aLength);
                int a = arr[i][j];
                arr[i][j] =arr[x][y];
                arr[x][y]= a;
            }
        }
        return arr;
    }
    public static void saveInit() throws Exception{
        File file = new File("saved.txt");
        if (file.exists()){
        try (FileInputStream fileInputStream = new FileInputStream(file);
             ObjectInput objectOutputStream = new ObjectInputStream(fileInputStream) {
             }){
            saves = (Saves)objectOutputStream.readObject();
        }
        }
    }
    public static void saveResult(Save obj) throws Exception{
        File file = new File("saved.txt");
        saves.objects.add(obj);
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)){
            objectOutputStream.writeObject(saves);
            System.out.println("saved under id: "+(int)saves.objects.stream().count());
        }
    }

    public static void main(String[] args) {
        int lvl = whatLVL();

        try {
            saveInit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int[][] arr = new int [lvl][lvl];
        boolean gameIsWork = false;
        gameIsWork = true;
        arr = mix(initArr(arr));
        printArr(arr);

        while (gameIsWork){
            String command = robot.next().toLowerCase();
            switch (command){
                case "/stop":
                    System.out.println("Game is stopped"); gameIsWork = false;break;
                case "/newgame":
                    System.out.println("New game is started");lvl = whatLVL();arr = new int [lvl][lvl];arr = mix(initArr(arr)); printArr(arr);break;
                case "/load":
                    System.out.println((int)saves.objects.stream().count() +" - count saves");
                    System.out.println("Enter id");
                    arr = saves.objects.get(Integer.parseInt(robot.next())-1).arr;
                    printArr(arr);
                    break;
                case "w":arr = move(zeroPos(arr),arr,-1,0);printArr(arr);break;
                case "a":arr = move(zeroPos(arr),arr,0,-1);printArr(arr);break;
                case "s":arr = move(zeroPos(arr),arr,1,0);printArr(arr); break;
                case "d":arr = move(zeroPos(arr),arr,0,1);printArr(arr);break;
                case "/save":
                    try {
                        saveResult(new Save((int)saves.objects.stream().count() ,arr));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    System.out.println("unknown command"); break;
            }

            if(isWin(arr)) {gameIsWork = false;
                System.out.println("Mission Complete");
            }
        }
        System.out.println("Game -");
    }
}
