package game;

import java.io.Serializable;

public class Save implements Serializable {
    int id;
    int[][] arr;

    public Save(int id,int[][] arr) {
        this.id = id;
        this.arr = arr;
    }
}
